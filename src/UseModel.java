import weka.classifiers.Classifier;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils.DataSource;

/**
 * Created by islamsalah on 4/29/17.
 */
public class UseModel {

    // use cmd line parameters :
    // -t dataset.arff -s J48Trained
    public static void main(String[] args) throws Exception{

        Instances data = DataSource.read(Utils.getOption("t", args));
        data.setClassIndex(data.numAttributes() - 1);

        Classifier cls =   (Classifier) weka.core.SerializationHelper.read(Utils.getOption("s", args)+".model");

        for(int i=0; i<data.numInstances(); i++) {
            int valueIndex = (int)cls.classifyInstance(data.instance(i));
            System.out.println(data.attribute(data.numAttributes() - 1).value(valueIndex));
        }
    }

}
